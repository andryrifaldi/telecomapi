﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Telecom.Domain;
using Telecom.Repository;
using Telecom.Service;

namespace Telecom.API
{
    public class IocConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            // register Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register repository. 
            RegisterRepository(builder);


            // Register all logics from BLL here.
            RegisterService(builder);

            // set the dependency resolver to be autofac.  
            Container = builder.Build();

            return Container;
        }

        private static void RegisterService(ContainerBuilder builder)
        {
            builder.RegisterType<CustomerService>().As<ICustomerService>();
            builder.RegisterType<PhoneService>().As<IPhoneService>();
        }

        private static void RegisterRepository(ContainerBuilder builder)
        {
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<PhoneRepository>().As<IPhoneRepository>();
        }

    }
}