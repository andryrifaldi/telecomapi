﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Telecom.Domain;
using Telecom.Service;

namespace Telecom.API.Controllers
{
    [RoutePrefix("api/Phone")]
    public class PhoneController : ApiController
    {
        IPhoneService _service;

        public PhoneController(IPhoneService service)
        {
            _service = service;
        }

        /// <summary>
        /// Gets a Phone by it's unique ID.
        /// </summary>
        /// <param name="id">The id of the Phone.</param>
        /// <returns>A single 'Phone' entity.</returns>
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Json(_service.Get(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get all customer phone
        /// </summary>
        /// <returns>Get all phones</returns>
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Json(_service.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets a list of customer phone by it's customer id.
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>List of customer phone</returns>
        [Route("GetPhoneByCustomerId/{id}")]
        public IHttpActionResult GetPhoneByCustomerId(int id)
        {
            try
            {
                return Json(_service.GetPhoneByCustomerId(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Activate customer phone number.
        /// </summary>
        /// <param name="id">Phone number id</param>
        /// <returns>activated == true</returns>
        [HttpPost]
        [Route("ActivatedPhoneNumber/{id}")]
        public IHttpActionResult ActivatedPhoneNumber(int id)
        {
            try
            {
                return Json(_service.ActivatedPhoneNumber(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



    }
}