﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Telecom.Domain;
using Telecom.Service;

namespace Telecom.API.Controllers
{
    [RoutePrefix("api/Customer")]
    public class CustomerController : ApiController
    {
        ICustomerService _service;

        public CustomerController(ICustomerService service){_service = service;}

        /// <summary>
        ///     Gets a Customer by it's unique ID.
        /// </summary>
        /// <param name="id">The id of the Customer.</param>
        /// <returns>A single 'Customer' entity.</returns>
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Json(_service.Get(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get all customer
        /// </summary>
        /// <returns>Get all customers</returns>
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Json(_service.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}