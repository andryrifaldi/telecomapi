﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telecom.Domain
{
    /// <summary>
    /// Phone entity
    /// </summary>
    public class Phone : BaseDomain
    {
        public string PhoneNumber { get; set; }
        public bool IsActivated { get; set; }

        // Set relational properties between class entity
        // Customer

        public int Customer_Id { get; set; }
    }
}
