﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telecom.Domain
{
    public class DummyObjectContext
    {
        public List<Customer> Customers;
        public List<Phone> Phones;

        public DummyObjectContext()
        {
            Customers = InitialiseCustomers();
            Phones = InitialisePhones();
        }

        private List<Customer> InitialiseCustomers()
        {
            List<Customer> customers = new List<Customer>();

            Customer customer1 = new Customer {
                Id = 1, FirstName = "Customer", LastName = "A",
                Phones = new List<Phone> {
                    new Phone { Id = 1, IsActivated = false, PhoneNumber = "+44123", Customer_Id = 1 },
                    new Phone { Id = 2, IsActivated = false, PhoneNumber = "+44886", Customer_Id = 1 },
                    new Phone { Id = 3, IsActivated = false, PhoneNumber = "+448006", Customer_Id = 1 },
                    new Phone { Id = 4, IsActivated = false, PhoneNumber = "+442286", Customer_Id = 1 } }
            };

            Customer customer2 = new Customer
            {
                Id = 2,
                FirstName = "Customer",
                LastName = "B",
                Phones = new List<Phone> {
                    new Phone { Id = 5, IsActivated = false, PhoneNumber = "+441122", Customer_Id = 2 },
                    new Phone { Id = 6, IsActivated = false, PhoneNumber = "+44886" , Customer_Id = 2 },
                    new Phone { Id = 7, IsActivated = false, PhoneNumber = "+44886", Customer_Id = 2} }
            };

            Customer customer3 = new Customer
            {
                Id = 3,
                FirstName = "Customer",
                LastName = "C",
                Phones = new List<Phone> {
                    new Phone { Id = 8, IsActivated = true, PhoneNumber = "+44124", Customer_Id = 3 },
                    new Phone { Id = 9, IsActivated = false, PhoneNumber = "+44886", Customer_Id = 3 } }
            };

            Customer customer4 = new Customer
            {
                Id = 4,
                FirstName = "Customer",
                LastName = "D",
                Phones = new List<Phone> {
                    new Phone { Id = 10, IsActivated = false, PhoneNumber = "+44125", Customer_Id = 4 },
                    new Phone { Id = 11, IsActivated = false, PhoneNumber = "+4413", Customer_Id = 4 } }
            };

            Customer customer5 = new Customer
            {
                Id = 5,
                FirstName = "Customer",
                LastName = "E",
                Phones = new List<Phone> {
                    new Phone { Id = 12, IsActivated = false, PhoneNumber = "+44126", Customer_Id = 5 },
                    new Phone { Id = 13, IsActivated = false, PhoneNumber = "+44133", Customer_Id = 5 },
                    new Phone { Id = 14, IsActivated = false, PhoneNumber = "+44177", Customer_Id = 5 } }
            };

            customers.Add(customer1);
            customers.Add(customer2);
            customers.Add(customer3);
            customers.Add(customer4);
            customers.Add(customer5);

            return customers;
        }

        private List<Phone> InitialisePhones()
        {
            List<Phone> phones = new List<Phone>();
            foreach (var customer in Customers)
                phones.AddRange(customer.Phones);
            
            return phones;
        }

    }
}
