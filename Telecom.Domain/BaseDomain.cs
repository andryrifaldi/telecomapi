﻿namespace Telecom.Domain
{
    /// <summary>
    /// Base domain class
    /// </summary>
    public class BaseDomain
    {
        public int Id { get; set; }
    }
}