﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telecom.Domain
{
    /// <summary>
    /// Customer Entity
    /// </summary>
    public class Customer : BaseDomain
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // Set relational properties between class entity
        // Phones
        public ICollection<Phone> Phones { get; set; }
    }
}
