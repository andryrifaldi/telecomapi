﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telecom.Domain;
using Telecom.Repository;
using Telecom.Service;
using System.Linq;
using Telecom.API.Controllers;
using System.Web.Http.Results;

namespace Telecom.Test
{
    [TestClass]
    public class PhoneTest
    {
        private static IPhoneRepository _repository;
        private static IPhoneService _service;


        [TestInitialize]
        public void Setup()
        {
            _repository = new PhoneRepository();
            _service = new PhoneService(_repository);
        }

        #region Repository
        [TestMethod]
        public void PhoneRepository_GetTest()
        {
            //Arrange
            var id = 1;

            //Act
            Phone phone = _repository.Get(id);

            //Assert
            Assert.IsNotNull(phone);
        }

        [TestMethod]
        public void PhoneRepository_GetAllTest()
        {
            //Arrange
            var phones = _repository.GetAll();

            if (phones.Count > 1)
            {
                //Act
                var phoneAscId = phones.OrderBy(x => x.Id).First().Id;
                var phoneDescId = phones.OrderByDescending(x => x.Id).First().Id;

                //Assert
                Assert.AreNotEqual(phoneAscId, phoneDescId);
            }
        }

        [TestMethod]
        public void PhoneRepository_GetPhoneByCustomerIdTest()
        {
            //Arrange
            var id = 1;

            //Act
            var phones = _repository.GetPhoneByCustomerId(id);

            Assert.AreNotEqual(0, phones.Count);

        }

        [TestMethod]
        public void PhoneRepository_ActivatedPhoneNumberTest()
        {
            //Arrange
            var id = 1;

            //Act
            var result = _repository.ActivatedPhoneNumber(id);

            //Assert
            Assert.IsTrue(result);
            }

        #endregion
        #region Service
        [TestMethod]
        public void PhoneService_GetTest()
        {
            //Arrange
            var id = 1;

            //Act
            Phone phone = _service.Get(id);

            //Assert
            Assert.IsNotNull(phone);
        }

        [TestMethod]
        public void PhoneServiceGetAllTest()
        {
            //Arrange
            var phones = _service.GetAll();

            if (phones.Count > 1)
            {
                //Act
                var phoneAscId = phones.OrderBy(x => x.Id).First().Id;
                var phoneDescId = phones.OrderByDescending(x => x.Id).First().Id;

                //Assert
                Assert.AreNotEqual(phoneAscId, phoneDescId);
            }
        }

        [TestMethod]
        public void PhoneService_GetPhoneByCustomerIdTest()
        {
            //Arrange
            var id = 1;

            //Act
            var phones = _service.GetPhoneByCustomerId(id);

            Assert.AreNotEqual(0, phones.Count);

        }

        [TestMethod]
        public void PhoneService_ActivatedPhoneNumberTest()
        {
            //Arrange
            var id = 1;

            //Act
            var result = _service.ActivatedPhoneNumber(id);

            //Assert
            Assert.IsTrue(result);
        }

        #endregion
        #region Api
        [TestMethod]
        public void PhoneApi_GetTest()
        {
            //Arrange
            var id = 1;
            var controller = new PhoneController(_service);

            //Act
            var result = controller.Get(id) as BadRequestErrorMessageResult;

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void PhoneApi_GetAllTest()
        {
            //Arrange
            var controller = new PhoneController(_service);

            //Act
            var result = controller.GetAll() as BadRequestErrorMessageResult;

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void PhoneApi_GetPhoneByCustomerIdTest()
        {
            //Arrange
            var id = 1;
            var controller = new PhoneController(_service);

            //Act
            var result = controller.GetPhoneByCustomerId(id) as BadRequestErrorMessageResult;

            Assert.IsNull(result);
        }

        [TestMethod]
        public void PhoneApi_ActivatedPhoneNumberTest()
        {
            //Arrange
            var id = 1;
            var controller = new PhoneController(_service);

            //Act
            var result = controller.ActivatedPhoneNumber(id) as BadRequestErrorMessageResult;

            Assert.IsNull(result);
        }



        #endregion
    }
}
