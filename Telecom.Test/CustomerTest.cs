﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telecom.Domain;
using Telecom.Repository;
using Telecom.Service;
using System.Linq;
using Telecom.API.Controllers;
using System.Web.Http.Results;

namespace Telecom.Test
{
    [TestClass]
    public class CustomerTest
    {
        private static ICustomerRepository _repository;
        private static ICustomerService _service;


        [TestInitialize]
        public void Setup()
        {
            _repository = new CustomerRepository();
            _service = new CustomerService(_repository);
        }

        #region Repository
        [TestMethod]
        public void CustomerRepository_GetTest()
        {
            //Arrange
            var id = 1;

            //Act
            Customer customer = _repository.Get(id);

            //Assert
            Assert.IsNotNull(customer);
        }

        [TestMethod]
        public void CustomerRepository_GetAllTest()
        {
            //Arrange
            var customers = _repository.GetAll();

            if (customers.Count > 1)
            {
                //Act
                var customerAscId = customers.OrderBy(x => x.Id).First().Id;
                var customerDescId = customers.OrderByDescending(x => x.Id).First().Id;

                //Assert
                Assert.AreNotEqual(customerAscId, customerDescId);
            }
        }


        #endregion
        #region Service
        [TestMethod]
        public void CustomerService_GetTest()
        {
            //Arrange
            var id = 1;

            //Act
            Customer customer = _service.Get(id);

            //Assert
            Assert.IsNotNull(customer);
        }

        [TestMethod]
        public void CustomerService_GetAllTest()
        {
            //Arrange
            var customers = _service.GetAll();

            if (customers.Count > 1)
            {
                //Act
                var customerAscId = customers.OrderBy(x => x.Id).First().Id;
                var customerDescId = customers.OrderByDescending(x => x.Id).First().Id;

                //Assert
                Assert.AreNotEqual(customerAscId, customerDescId);
            }
        }

        #endregion
        #region Api
        [TestMethod]
        public void CustomerApi_GetTest()
        {
            //Arrange
            var id = 1;
            var controller = new CustomerController(_service);

            //Act
            var result = controller.Get(id) as BadRequestErrorMessageResult;

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void CustomerApi_GetAllTest()
        {
            //Arrange
            var controller = new CustomerController(_service);

            //Act
            var result = controller.GetAll() as BadRequestErrorMessageResult;

            //Assert
            Assert.IsNull(result);
        }

        #endregion
    }
    }
