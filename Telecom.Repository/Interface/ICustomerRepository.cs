﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;

namespace Telecom.Repository
{
    public interface ICustomerRepository
    {
        List<Customer> GetAll();
        Customer Get(int id);
    }
}
