﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;

namespace Telecom.Repository
{
    public interface IPhoneRepository
    {
        List<Phone> GetAll();
        Phone Get(int id);
        List<Phone> GetPhoneByCustomerId(int customerId);
        bool ActivatedPhoneNumber(int id);

    }
}
