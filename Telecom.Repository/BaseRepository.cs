﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telecom.Domain;

namespace Telecom.Repository
{
    public class BaseRepository 
    {
        #region Constructor

        protected readonly DummyObjectContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public BaseRepository()
        {
            db = new DummyObjectContext();
        }

        #endregion

    }
}
