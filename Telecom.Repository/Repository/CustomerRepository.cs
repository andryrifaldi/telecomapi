﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;

namespace Telecom.Repository
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public CustomerRepository() : base(){}

        #endregion

        /// <summary>
        /// Get all customer
        /// </summary>
        /// <returns>Customer List</returns>
        public List<Customer> GetAll()
        {
            return db.Customers;
        }

        /// <summary>
        /// Get customer by unique id/
        /// </summary>
        /// <param name="id">Customer unique id</param>
        /// <returns>Single `Customer` entity.</returns>
        public Customer Get(int id)
        {
            var result = db.Customers.FirstOrDefault(x => x.Id == id);
            return result;
        }

    }
}
