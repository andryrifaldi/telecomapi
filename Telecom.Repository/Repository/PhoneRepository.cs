﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;

namespace Telecom.Repository
{
    public class PhoneRepository : BaseRepository, IPhoneRepository
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneRepository"/> class.
        /// </summary>
        public PhoneRepository() : base(){}

        #endregion

        /// <summary>
        /// Get all customer phone
        /// </summary>
        /// <returns>Get all phones</returns>
        public List<Phone> GetAll()
        {
            return db.Phones;
        }

        /// <summary>
        /// Gets a Phone by it's unique ID.
        /// </summary>
        /// <param name="id">The id of the Phone.</param>
        /// <returns>A single 'Phone' entity.</returns>
        public Phone Get(int id)
        {
            var result = db.Phones.FirstOrDefault(x => x.Id == id);
            return result;
        }

        /// <summary>
        /// Gets a list of customer phone by it's customer id.
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>List of customer phone</returns>
        public List<Phone> GetPhoneByCustomerId(int customerId)
        {
            var result = db.Phones.Where(x => x.Customer_Id == customerId).ToList();
            return result;
        }

        /// <summary>
        /// Activate customer phone number.
        /// </summary>
        /// <param name="id">Phone number id</param>
        /// <returns>activated == true</returns>
        public bool ActivatedPhoneNumber(int id)
        {
            var result = db.Phones.FirstOrDefault(x => x.Id == id);
            if (result != null)
                return result.IsActivated = true;
            else
                return false;
        }

    }
}
