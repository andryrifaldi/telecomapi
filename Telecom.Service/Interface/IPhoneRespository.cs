﻿using System.Collections.Generic;
using Telecom.Domain;

namespace Telecom.Service
{
    public interface IPhoneService
    {
        List<Phone> GetAll();
        Phone Get(int id);
        List<Phone> GetPhoneByCustomerId(int customerId);
        bool ActivatedPhoneNumber(int id);

    }
}
