﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;
using Telecom.Repository;

namespace Telecom.Service
{
    public class CustomerService : ICustomerService
    {
        ICustomerRepository _repository;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        public CustomerService(ICustomerRepository repository)
        {
            _repository = repository;
        }

        #endregion

        /// <summary>
        ///     Gets a Customer by it's unique ID.
        /// </summary>
        /// <param name="id">The id of the Customer.</param>
        /// <returns>A single 'Customer' entity.</returns>
        public Customer Get(int id)
        {
            return _repository.Get(id);
        }

        /// <summary>
        /// Get all customer
        /// </summary>
        /// <returns>Get all customers</returns>
        public List<Customer> GetAll()
        {
            return _repository.GetAll();
        }
    }
}
