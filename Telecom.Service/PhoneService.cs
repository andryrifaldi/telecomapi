﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telecom.Domain;
using Telecom.Repository;

namespace Telecom.Service
{
    public class PhoneService : IPhoneService
    {
        private IPhoneRepository _repository;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneService"/> class.
        /// </summary>
        public PhoneService(IPhoneRepository repository)
        {
            _repository = repository;
        }
        #endregion

        /// <summary>
        /// Activate customer phone number.
        /// </summary>
        /// <param name="id">Phone number id</param>
        /// <returns>activated == true</returns>
        public bool ActivatedPhoneNumber(int id)
        {
            return _repository.ActivatedPhoneNumber(id);
        }

        /// <summary>
        /// Gets a Phone by it's unique ID.
        /// </summary>
        /// <param name="id">The id of the Phone.</param>
        /// <returns>A single 'Phone' entity.</returns>
        public Phone Get(int id)
        {
            return _repository.Get(id);
        }

        /// <summary>
        /// Get all customer phone
        /// </summary>
        /// <returns>Get all phones</returns>
        public List<Phone> GetAll()
        {
            return _repository.GetAll();
        }

        /// <summary>
        /// Gets a list of customer phone by it's customer id.
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>List of customer phone</returns>
        public List<Phone> GetPhoneByCustomerId(int customerId)
        {
            return _repository.GetPhoneByCustomerId(customerId);
        }
    }
}
